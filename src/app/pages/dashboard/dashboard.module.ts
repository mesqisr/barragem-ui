import { DashboardRoutingModule } from './dashboard-routing.module';
import { NgModule } from '@angular/core';

import {IMaskModule} from 'angular-imask';
import {ButtonModule} from 'primeng/button';
import {DashboardComponent} from './dashboard.component';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
    declarations: [DashboardComponent],

    imports: [
        DashboardRoutingModule,
        IMaskModule,
        ButtonModule,
        SharedModule
    ]
})
export class DashboardModule { }
