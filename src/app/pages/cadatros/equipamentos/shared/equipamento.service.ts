import { Injectable, Injector } from '@angular/core';
import {Equipamento} from './equipamento.model';
import { BaseResourceService } from 'src/app/core/services/base-resource.service';

@Injectable({
  providedIn: 'root'
})
export class EquipamentoService extends BaseResourceService<Equipamento> {

  constructor(protected injector: Injector) {
    super('/equipamentos', injector, Equipamento.fromJson);
  }
}
