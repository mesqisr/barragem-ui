import {BaseResourceModelDirective} from '../../../../core/base/model/base-resource.model';
import {CategoriaEnum} from '../../../../shared/models/enums/categoria.enum';

export class Equipamento extends BaseResourceModelDirective {
  constructor(
    public id?: any,
    public descricao?: string,
    public quantidade?: string,
    public categoria?: CategoriaEnum

  ) {
    super();
  }

  static fromJson(jsonData: any): Equipamento {
    return Object.assign(new Equipamento() , jsonData);
  }

}
