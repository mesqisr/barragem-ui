import { Component } from '@angular/core';
import {BaseResourceListDirective} from '../../../../core/base/base-resource-list.directive';
import {EquipamentoService} from '../shared/equipamento.service';
import {Equipamento} from '../shared/equipamento.model';
import {BaseMiniCrudListDirective} from '../../../../core/base/base-mini-crud-list.directive';
import {ConfirmationService} from 'primeng/api';
import {FormularioService} from '../../../../shared/services/formulario.service';
import {CategoriaEnum} from '../../../../shared/models/enums/categoria.enum';

@Component({
  selector: 'app-equipamento-list',
  templateUrl: './equipamento-list.component.html',
  styleUrls: ['./equipamento-list.component.scss']
})
export class EquipamentoListComponent extends BaseMiniCrudListDirective<Equipamento> {

  public categoriaEnum = CategoriaEnum;

  // tslint:disable-next-line:max-line-length
  constructor(protected formularioSerivce: FormularioService , protected confirmationService: ConfirmationService, protected equipamentoService: EquipamentoService) {
    super(formularioSerivce, confirmationService, equipamentoService);
  }

}
