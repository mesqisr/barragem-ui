import { SharedModule } from './../../../shared/shared.module';
import { EquipamentosRoutingModule } from './equipamentos-routing.module';
import { NgModule } from '@angular/core';

import {IMaskModule} from 'angular-imask';
import {ButtonModule} from 'primeng/button';
import {EquipamentoListComponent} from './equipamento-list/equipamento-list.component';
import {EquipamentoFormComponent} from './equipamento-form/equipamento-form.component';

@NgModule({
    declarations: [EquipamentoListComponent, EquipamentoFormComponent],
    exports: [
        EquipamentoListComponent
    ],
    imports: [
        EquipamentosRoutingModule,
        IMaskModule,
        ButtonModule,
        SharedModule
    ]
})
export class EquipamentosModule { }
