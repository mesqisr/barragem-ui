import { Component, Injector, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import {Equipamento} from '../shared/equipamento.model';
import {EquipamentoService} from '../shared/equipamento.service';
import {BaseMiniCrudFormDirective} from '../../../../core/base/base-mini-crud-form.directive';
import {CategoriaEnum} from '../../../../shared/models/enums/categoria.enum';

@Component({
  selector: 'app-equipamento-form',
  templateUrl: './equipamento-form.component.html',
  styleUrls: ['./equipamento-form.component.scss']
})
export class EquipamentoFormComponent extends BaseMiniCrudFormDirective<Equipamento> {

  categorias = CategoriaEnum;

   constructor(protected ativoService: EquipamentoService,
               protected injector: Injector
           ) {
    super(injector, new Equipamento(), ativoService, Equipamento.fromJson);
  }

  protected buildResourceForm(): void {
     this.resourceForm = this.formBuilder.group({
       id : [null],
       descricao: [null, [Validators.required , Validators.maxLength(200)]],
       categoria: [null, [Validators.required]],
       quantidade: [null, [Validators.required, Validators.min(0), Validators.max(999)]],
    });
  }

  protected creationPageTitle(): string {
    return 'Cadastro de Equipamento';
  }

  protected editionPageTitle(): string {
    const ativoTitulo: string  = this.resource.descricao || '' ;
    return 'Editando Equipamento : ' + ativoTitulo;
  }

}


