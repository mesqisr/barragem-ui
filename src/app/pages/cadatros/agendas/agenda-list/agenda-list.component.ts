import { Component } from '@angular/core';
import {BaseResourceListDirective} from '../../../../core/base/base-resource-list.directive';
import {AgendaService} from '../shared/agenda.service';
import {Agenda} from '../shared/agenda.model';
import {ConfirmationService} from 'primeng/api';
import {FormularioService} from '../../../../shared/services/formulario.service';

@Component({
  selector: 'app-agenda-list',
  templateUrl: './agenda-list.component.html',
  styleUrls: ['./agenda-list.component.scss']
})
export class AgendaListComponent extends BaseResourceListDirective<Agenda> {

  constructor(protected formularioSerivce: FormularioService , protected confirmationService: ConfirmationService, protected agendaService: AgendaService) {
    super(formularioSerivce, confirmationService, agendaService);
  }

}
