import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AgendaFormComponent} from './agenda-form/agenda-form.component';

const routes: Routes = [
  {path: '' , component: AgendaFormComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AgendasRoutingModule { }
