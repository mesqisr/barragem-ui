import { SharedModule } from './../../../shared/shared.module';
import { AgendasRoutingModule } from './agendas-routing.module';
import {LOCALE_ID, NgModule} from '@angular/core';

import {IMaskModule} from 'angular-imask';
import {ButtonModule} from 'primeng/button';
import {AgendaListComponent} from './agenda-list/agenda-list.component';
import {AgendaFormComponent} from './agenda-form/agenda-form.component';

import {DialogModule} from 'primeng/dialog';
import {CalendarModule} from 'primeng/calendar';
import {ComplexoMinerario} from '../complexos/shared/complexo.model';


import { registerLocaleData } from '@angular/common';
import ptBr from '@angular/common/locales/pt';

registerLocaleData(ptBr);

@NgModule({
  declarations: [AgendaListComponent, AgendaFormComponent],
  imports: [
    // AgendasRoutingModule,
    IMaskModule,
    ButtonModule,
    SharedModule,
    DialogModule,
    CalendarModule
  ],
  providers: [
    {provide: LOCALE_ID, useValue: 'pt'}
    ]
})
export class AgendasModule { }
