import {Component, Injector, ViewChild} from '@angular/core';
import {Validators} from '@angular/forms';
import {Agenda} from '../shared/agenda.model';
import {AgendaService} from '../shared/agenda.service';
import {BaseMiniCrudFormDirective} from '../../../../core/base/base-mini-crud-form.directive';

import {FullCalendar} from 'primeng/fullcalendar';

import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import {TipoEventoEnum} from '../../../../shared/models/enums/tipo-evento.enum';
import {Ativo} from '../../ativos/shared/ativo.model';
import toastr from 'toastr';
import {ConfirmationService} from 'primeng/api';
import {AtivoService} from '../../ativos/shared/ativo.service';
import {ComplexoMinerario} from '../../complexos/shared/complexo.model';
import {ComplexoService} from '../../complexos/shared/complexo.service';
import {map} from 'rxjs/operators';

import * as moment from 'moment';
import 'moment/locale/pt-br';

@Component({
  selector: 'app-agenda-form',
  templateUrl: './agenda-form.component.html',
  styleUrls: ['./agenda-form.component.scss']
})
export class AgendaFormComponent extends BaseMiniCrudFormDirective<Agenda> {

  constructor(protected agendaService: AgendaService,
              private ativoService: AtivoService,
              protected confirmationService: ConfirmationService,
              protected injector: Injector,
              private complexoSevice: ComplexoService
  ) {
    super(injector, new Agenda(), agendaService, Agenda.fromJson);
  }

  @ViewChild('calendar') private calendar: FullCalendar;
  tipoEventos = TipoEventoEnum;

  ativos: Array<Ativo>;

  display = false;

  events: any[] = [];

  options: any;
  pt = {
    firstDayOfWeek: 0,
    dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
    dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
    dayNamesMin: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sa'],
    monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho',
      'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
    monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
    today: 'Hoje',
    clear: 'Limpar'
  };

  public complexoMinerario: ComplexoMinerario;

  // tslint:disable-next-line:typedef
  ngOnInit() {
    this.buildResourceForm();
    this.buscarComplexo();
    this.loadAtivos();
    this.options = {
      plugins: [dayGridPlugin, timeGridPlugin, interactionPlugin],
      defaultDate: new Date(),
      header: {
        left: 'prev,next',
        center: 'title',
        right: 'dayGridMonth,timeGridWeek,timeGridDay'
      },
      eventClick: (info) => {
        this.findByEvent(+info.event.id);
      }
    };
  }

  public getNomeComplexo(): string {
    return `AGENDA DO ${this.complexoMinerario?.nome.toLocaleUpperCase()}`;
  }

  // tslint:disable-next-line:typedef
  private async buscarComplexo() {
      let id;
      this.route.params.pipe(map(p => p.id)).subscribe(value => id = value );
      await this.complexoSevice.getById(id).subscribe(response => {
        this.complexoMinerario = response;
      });

      this.items =  await this.agendaService.getAgendaPorComplexo(id);
      this.montarCalendario();
    }

  private montarCalendario(): void {


    this.events = [];

    this.items.forEach(item => {

         const evento  = {
          id: item.id,
          title: item.evento,
          start: item.dataInicio,
          end: item.dataConclusao
      };

         this.events.push(evento);

     });

  }


  protected buildResourceForm(): void {
    this.resourceForm = this.formBuilder.group({
      id: [null],
      evento: [null, [Validators.required, Validators.maxLength(200)]],
      descricao: [null, [Validators.required, Validators.maxLength(200)]],
      dataInicio: [null, [Validators.required]],
      dataConclusao: [null, [Validators.required]],
      tipoEvento: [null, [Validators.required]],
      ativo: [null, [Validators.required]],
      complexoMinerario: [null]
    });
  }

  protected setPageTitle(): void {
    if (this.resourceForm.get('id').value == null) {
      this.pageTitle = this.creationPageTitle();
    } else {
      this.pageTitle = this.editionPageTitle();
    }
  }

  protected creationPageTitle(): string {
    return 'Novo Evento';
  }

  protected editionPageTitle(): string {
    const agendaEvento: string = this.resourceForm.get('evento').value || '';
    return 'Editando : ' + agendaEvento;
  }

  showDialog(): void {
    this.clearForm();
    this.display = true;
  }


  submitForm(validateForm: boolean = true, resetFormUponSuccess: boolean = true): void {

    this.resourceForm.get('complexoMinerario').setValue(this.complexoMinerario);

    if (this.resourceForm.get('dataInicio').value !== null) {
        const date = moment(this.resourceForm.get('dataInicio').value).format('YYYY-MM-DD');
        this.resourceForm.get('dataInicio').setValue(date);
    }

    if (this.resourceForm.get('dataConclusao').value !== null) {
       const date = moment(this.resourceForm.get('dataConclusao').value).format('YYYY-MM-DD');
       this.resourceForm.get('dataConclusao').setValue(date);
    }

    super.submitForm(validateForm, resetFormUponSuccess);

    debugger;
  }


  protected executaPreSetItem(): void {

    if (this.resource.dataInicio) {

       debugger;

      const newDate = new Date(this.resource.dataInicio);
      newDate.setDate(newDate.getDate() + 1);
      this.resource.dataInicio = newDate;
    }

    if (this.resource.dataConclusao) {
        const newDate = new Date(this.resource.dataConclusao);
        newDate.setDate(newDate.getDate() + 1);
        this.resource.dataConclusao = newDate;
    }

  }

  private loadAtivos(): void {
    this.ativoService.getAll().subscribe(
      response => this.ativos = response
    );
  }

  public selectComparator(t1: Ativo, t2: Ativo): boolean {
    return super.selectComparator(t1, t2);
  }

  public removerEvento(evento): void {

    this.confirmationService.confirm({
      message: 'Deseja realmente excluir esse evento ?',
      accept: () => {

        this.loadingResources = true;

        this.agendaService.delete(evento.id).subscribe(
          () => {
            toastr.success('Solicitação processada com sucesso!');
            this.loadingResources = false;
            this.display = false;
            this.clearForm();
            this.events = this.events.filter(element => element.id !== evento.id);
            this.items = this.items.filter(element => element.id !== evento.id);
          },
          () => {
            toastr.error('Ocorreu um erro ao processar a sua solicitação');
            this.loadingResources = false;
          });
      }
    });
  }

  private findByEvent(id: number): void {

    const evento = this.items.find(e => e.id === id);

    if (evento) {

      if (evento.dataInicio) {
        //const newDate = new Date(evento.dataInicio);
        //newDate.setDate(newDate.getDate());
        //evento.dataInicio = newDate;
      }

      if (evento.dataConclusao) {
        //const newDate = new Date(evento.dataConclusao);
        //newDate.setDate(newDate.getDate());
        //evento.dataConclusao = newDate;
      }

      this.resourceForm.patchValue(Object.assign(new Agenda(), evento));
    }

    this.display = true;
  }

  protected actionsForSuccess(resource: any, resetFormUponSuccess: boolean = true): void {

    const newDateInicio = new Date(resource.dataInicio);
    newDateInicio.setDate(newDateInicio.getDate() + 1);
    resource.dataInicio = newDateInicio;

    const newDateConclusao = new Date(resource.dataConclusao);
    newDateConclusao.setDate(newDateConclusao.getDate() + 1);
    resource.dataConclusao = newDateConclusao;


    // teste aqui update

    debugger;

   this.montarCalendario();

    super.actionsForSuccess(resource, resetFormUponSuccess);

    this.display = false;

  }

}


