import { Injectable, Injector } from '@angular/core';
import {Agenda} from './agenda.model';
import { BaseResourceService } from 'src/app/core/services/base-resource.service';
import {Observable} from 'rxjs';
import {RelatorioDto} from '../../../../shared/models/dto/relatorioDto';

@Injectable({
  providedIn: 'root'
})
export class AgendaService extends BaseResourceService<Agenda> {

  private URL_CONTEXT = '/barragem/v1';

  constructor(protected injector: Injector) {
    super('/agendas', injector, Agenda.fromJson);
  }

  getAgendaPorComplexo(idComplexo: string): Promise<Agenda[]> {
    const result = this.http.get<Agenda[]>(`${this.URL_CONTEXT}/agendas/complexo?id=${idComplexo}`).toPromise();
    return Promise.resolve(result);
  }

}
