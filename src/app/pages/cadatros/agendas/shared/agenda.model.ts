import {BaseResourceModelDirective} from '../../../../core/base/model/base-resource.model';
import {TipoEventoEnum} from '../../../../shared/models/enums/tipo-evento.enum';
import {Ativo} from '../../ativos/shared/ativo.model';
import {ComplexoMinerario} from '../../complexos/shared/complexo.model';

export class Agenda extends BaseResourceModelDirective {
  constructor(
    public id?: any,
    public evento?: string,
    public descricao?: string,
    public tipoEvento?: TipoEventoEnum,
    public dataInicio?: Date,
    public dataConclusao?: Date,
    public ativo?: Ativo,
    public complexoMinerario?: ComplexoMinerario
  ) {
    super();
  }

  static fromJson(jsonData: any): Agenda {
    return Object.assign(new Agenda() , jsonData);
  }

}
