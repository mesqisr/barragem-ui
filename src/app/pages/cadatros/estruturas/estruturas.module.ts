import { SharedModule } from './../../../shared/shared.module';
import { EstruturasRoutingModule } from './estruturas-routing.module';
import { NgModule } from '@angular/core';

import {IMaskModule} from 'angular-imask';
import {ButtonModule} from 'primeng/button';
import {EstruturaFormComponent} from './estrutura-form/estrutura-form.component';
import {EstruturaListComponent} from './estrutura-list/estrutura-list.component';

@NgModule({
  declarations: [EstruturaFormComponent, EstruturaListComponent],
  exports: [
    EstruturaFormComponent
  ],
  imports: [
    EstruturasRoutingModule,
    IMaskModule,
    ButtonModule,
    SharedModule
  ]
})
export class EstruturasModule { }
