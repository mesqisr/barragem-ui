import { Component } from '@angular/core';
import {EstruturaService} from '../shared/estrutura.service';
import {Estrutura} from '../shared/estrutura.model';
import {TipoEstruturaEnum} from '../../../../shared/models/enums/tipo-estrutura-enum';
import {SituacaoEnum} from '../../../../shared/models/enums/situacao.enum';
import {TipoRiscoEnum} from '../../../../shared/models/enums/tipo-risco.enum';
import {NivelEmergenciaEnum} from '../../../../shared/models/enums/nivel-emergencia.enum';
import {ConfirmationService} from 'primeng/api';
import {BaseMiniCrudListDirective} from '../../../../core/base/base-mini-crud-list.directive';

import toastr from 'toastr';
import {FormularioService} from '../../../../shared/services/formulario.service';

@Component({
  selector: 'app-estrutura-list',
  templateUrl: './estrutura-list.component.html',
  styleUrls: ['./estrutura-list.component.scss']
})
export class EstruturaListComponent extends BaseMiniCrudListDirective<Estrutura> {

  public situacaoEnum = SituacaoEnum;
  public tipoEstrutura = TipoEstruturaEnum;
  public tipoRisco = TipoRiscoEnum;
  public nivelEmergencia = NivelEmergenciaEnum;


  constructor(protected formularioSerivce: FormularioService, protected estruturaService: EstruturaService,  protected confirmationService: ConfirmationService) {
    super(formularioSerivce, confirmationService, estruturaService);
  }

  protected deleteResource(resource): void {
      const indexElement = this.items.indexOf(resource);
      this.items.splice(indexElement, 1);
      toastr.success('Solicitação processada com sucesso!');
  }
}
