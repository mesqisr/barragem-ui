import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {EstruturaListComponent} from './estrutura-list/estrutura-list.component';
import {EstruturaFormComponent} from './estrutura-form/estrutura-form.component';

const routes: Routes = [
  {path: '' , component: EstruturaListComponent},
  {path: 'new' , component: EstruturaFormComponent},
  {path: ':id/edit' , component: EstruturaFormComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EstruturasRoutingModule { }
