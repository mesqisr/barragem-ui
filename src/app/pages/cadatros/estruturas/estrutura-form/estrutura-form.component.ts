import { Component, Injector, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import {Estrutura} from '../shared/estrutura.model';
import {EstruturaService} from '../shared/estrutura.service';
import {BaseResourceFormDirective} from '../../../../core/base/base-resource-form.directive';
import {SituacaoEnum} from '../../../../shared/models/enums/situacao.enum';
import {TipoEstruturaEnum} from '../../../../shared/models/enums/tipo-estrutura-enum';
import {TipoRiscoEnum} from '../../../../shared/models/enums/tipo-risco.enum';
import {NivelEmergenciaEnum} from '../../../../shared/models/enums/nivel-emergencia.enum';
import {BaseMiniCrudFormDirective} from '../../../../core/base/base-mini-crud-form.directive';

@Component({
  selector: 'app-estrutura-form',
  templateUrl: './estrutura-form.component.html',
  styleUrls: ['./estrutura-form.component.scss']
})
export class EstruturaFormComponent extends BaseMiniCrudFormDirective<Estrutura> {

  public situacaoEnum = SituacaoEnum;
  public tipoEstrutura = TipoEstruturaEnum;
  public tipoRisco = TipoRiscoEnum;
  public nivelEmergencia = NivelEmergenciaEnum;

  constructor(protected complexoMInerarioService: EstruturaService,
              protected injector: Injector
           ) {
    super(injector, new Estrutura(), complexoMInerarioService, Estrutura.fromJson);
  }

  ngOnInit(): void {
    this.buildResourceForm();
  }

  protected buildResourceForm(): void {
     this.resourceForm = this.formBuilder.group({
       id : [null],
       tipo: [null, [Validators.required]],
       situacao: [null, [Validators.required]],
       descricao: [null, [Validators.required , Validators.maxLength(200)]],
       linkPlanoAcao: [null, [Validators.required , Validators.maxLength(200)]],
       inseridaPoliticaNacional: [false, [Validators.required]],
       danoPotencial: [null, [Validators.required]],
       categoriaRisco: [null, [Validators.required]],
       nivelEmergencia: [null, [Validators.required]]
    });
  }

  protected creationPageTitle(): string {
    return 'Cadastro de Novo Complexo Minerario';
  }

  protected editionPageTitle(): string {
    const complexoNome: string  = this.resource.descricao || '' ;
    return 'Editando Complexo Minerario : ' + complexoNome;
  }


  protected createResource(resetFormUponSuccess = true): void {
    const estrutura = this.jsonDataToResourceFn(this.resourceForm.value);
    this.items = [].concat(this.items, estrutura);
    this.actionsForSuccess(estrutura, resetFormUponSuccess);
  }

  protected updateResource(resetFormUponSuccess = true): void {
    const estrutura = this.jsonDataToResourceFn(this.resourceForm.value);
    this.items.splice(this.indexResource, 1, estrutura);
    this.items = [].concat(this.items);
    this.actionsForSuccess(estrutura, resetFormUponSuccess);
  }

  protected clearForm(): void {
    this.resource = Object.create(null);
    this.currentResource = Object.create(null);
    this.resourceForm.reset({inseridaPoliticaNacional : false});
    this.submittingForm = false;
    this.indexResource = -1;
  }

}


