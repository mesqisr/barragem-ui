import { Injectable, Injector } from '@angular/core';
import {Estrutura} from './estrutura.model';
import { BaseResourceService } from 'src/app/core/services/base-resource.service';

@Injectable({
  providedIn: 'root'
})
export class EstruturaService extends BaseResourceService<Estrutura> {

  constructor(protected injector: Injector) {
    super('/estruturas', injector, Estrutura.fromJson);
  }
}
