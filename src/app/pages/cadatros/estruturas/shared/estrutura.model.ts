import {BaseResourceModelDirective} from '../../../../core/base/model/base-resource.model';
import {SituacaoEnum} from '../../../../shared/models/enums/situacao.enum';
import {EstadoEnum} from '../../../../shared/models/enums/estado.enum';
import {TipoEstruturaEnum} from '../../../../shared/models/enums/tipo-estrutura-enum';
import {TipoRiscoEnum} from '../../../../shared/models/enums/tipo-risco.enum';
import {NivelEmergenciaEnum} from '../../../../shared/models/enums/nivel-emergencia.enum';

export class Estrutura extends BaseResourceModelDirective {
  constructor(
    public id?: any,
    public tipo?: TipoEstruturaEnum,
    public situacao?: SituacaoEnum,
    public descricao?: string,
    public linkPlanoAcao?: string,
    public inseridaPoliticaNacional?: boolean,
    public danoPotencial?: TipoRiscoEnum,
    public categoriaRisco?: TipoRiscoEnum,
    public nivelEmergencia?: NivelEmergenciaEnum,
  ) {
    super();
  }

  static fromJson(jsonData: any): Estrutura {
    return Object.assign(new Estrutura() , jsonData);
  }

}
