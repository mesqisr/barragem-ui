import {BaseResourceModelDirective} from '../../../../core/base/model/base-resource.model';
import {SituacaoEnum} from '../../../../shared/models/enums/situacao.enum';
import {EstadoEnum} from '../../../../shared/models/enums/estado.enum';

export class ComplexoMinerario extends BaseResourceModelDirective {
  constructor(
    public id?: any,
    public nome?: string,
    public uf?: EstadoEnum,
    public municipio?: string,
    public situacao?: SituacaoEnum,
    public estruturas?: any[],
    public equipamentos?: any[],
    // public agendas?: any[]
  ) {
    super();
  }

  static fromJson(jsonData: any): ComplexoMinerario {
    return Object.assign(new ComplexoMinerario() , jsonData);
  }

}
