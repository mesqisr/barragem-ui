import { Injectable, Injector } from '@angular/core';
import { ComplexoMinerario } from './complexo.model';
import { BaseResourceService } from 'src/app/core/services/base-resource.service';
import {Observable} from 'rxjs';
import {RelatorioDto} from '../../../../shared/models/dto/relatorioDto';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ComplexoService extends BaseResourceService<ComplexoMinerario> {

  private URL_CONTEXT = '/barragem/v1';

  constructor(protected injector: Injector) {
    super('/complexosMinerarios', injector, ComplexoMinerario.fromJson);
  }

  getRelatorio(idComplexo: number): Observable<RelatorioDto> {

    return this.http.get<RelatorioDto>(`${this.URL_CONTEXT}/complexosMinerarios/relatorio/${idComplexo}`).pipe(

      map((response) => {

        const relatorio: RelatorioDto = {
          relatorio: this.convertBase64toBlob(response.relatorio, 'application/pdf'),
          nomeArquivo: response.nomeArquivo
        };
        return relatorio;
      }));
  }

  private convertBase64toBlob = (content, contentType) => {
    contentType = contentType || '';
    const sliceSize = 512;
    // method which converts base64 to binary
    const byteCharacters = window.atob(content);

    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);
      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }
    const blob = new Blob(byteArrays, {
      type: contentType
    }); // statement which creates the blob
    return blob;
  }
}
