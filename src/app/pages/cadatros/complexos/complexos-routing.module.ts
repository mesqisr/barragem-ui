import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ComplexoListComponent} from './complexo-list/complexo-list.component';
import {ComplexoFormComponent} from './complexo-form/complexo-form.component';
import {AgendaFormComponent} from '../agendas/agenda-form/agenda-form.component';
import {AgendasModule} from '../agendas/agendas.module';

const routes: Routes = [
  {path: '' , component: ComplexoListComponent},
  {path: 'new' , component: ComplexoFormComponent},
  {path: ':id/edit' , component: ComplexoFormComponent},
  {path: ':id/agenda' , component: AgendaFormComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes), AgendasModule],
  exports: [RouterModule]
})
export class ComplexosRoutingModule { }
