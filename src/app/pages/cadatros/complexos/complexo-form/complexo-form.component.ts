import {Component, Injector, OnInit, ViewChild} from '@angular/core';
import {Validators} from '@angular/forms';
import {ComplexoMinerario} from '../shared/complexo.model';
import {ComplexoService} from '../shared/complexo.service';
import {BaseResourceFormDirective} from '../../../../core/base/base-resource-form.directive';
import {EstadoEnum} from '../../../../shared/models/enums/estado.enum';
import {SituacaoEnum} from '../../../../shared/models/enums/situacao.enum';
import {MenuItem, PrimeNGConfig} from 'primeng/api';
import {EstruturaFormComponent} from '../../estruturas/estrutura-form/estrutura-form.component';
import {Equipamento} from '../../equipamentos/shared/equipamento.model';
import {EquipamentoService} from '../../equipamentos/shared/equipamento.service';
import {CategoriaEnum} from '../../../../shared/models/enums/categoria.enum';

@Component({
  selector: 'app-complexo-form',
  templateUrl: './complexo-form.component.html',
  styleUrls: ['./complexo-form.component.scss']
})
export class ComplexoFormComponent extends BaseResourceFormDirective<ComplexoMinerario> {

  @ViewChild('estruturaForm') private estruturaForm: EstruturaFormComponent;
  sourceEquipamentos: Equipamento[];
  targetEquipamentos: Equipamento[];

  items: MenuItem[];
  step = 0;
  situacao = SituacaoEnum;
  categoriaEquipamento = CategoriaEnum;
  uf = EstadoEnum;

  constructor(protected complexoMInerarioService: ComplexoService,
              private equipamentoService: EquipamentoService,
              private primengConfig: PrimeNGConfig,
              protected injector: Injector
  ) {
    super(injector, new ComplexoMinerario(), complexoMInerarioService, ComplexoMinerario.fromJson);
  }

  // tslint:disable-next-line:typedef
  ngOnInit() {
    this.items = [{
      label: 'Dados do Complexo',
      command: (event: any) => {
        this.step = 0;

      }
    },
      {
        label: 'Estruturas',
        command: (event: any) => {
          this.step = 1;

        }
      },
      {
        label: 'Equipamentos',
        command: (event: any) => {
          this.step = 2;

        }
      }
    ];
    this.buscarEquipamentos();
    super.ngOnInit();
  }

  submitForm(): void {
    const estrtuturas = this.estruturaForm.items;
    this.resourceForm.get('estruturas').setValue(estrtuturas);
    this.resourceForm.get('equipamentos').setValue(this.targetEquipamentos);
    super.submitForm();
  }

  protected buildResourceForm(): void {
    this.resourceForm = this.formBuilder.group({
      id: [null],
      nome: [null, [Validators.required, Validators.maxLength(200)]],
      uf: [null, [Validators.required]],
      municipio: [null, [Validators.required, Validators.maxLength(200)]],
      situacao: [null, [Validators.required]],
      estruturas: [null],
      equipamentos: [null]
    });
  }

  protected creationPageTitle(): string {
    return 'Cadastro de Novo Complexo Minerario';
  }

  protected editionPageTitle(): string {
    const complexoNome: string = this.resource.nome || '';
    return 'Editando Complexo Minerario : ' + complexoNome;
  }

  clickNext(nextStep): void {
    if (nextStep === 1) {

    }
    if (nextStep === 2) {

    }
    if (nextStep === 3) {
      return;
    }
    this.step = nextStep;

  }

  clickPrevious(previousStep): void {
    this.step = previousStep;
  }


  private async buscarEquipamentos(): Promise<void> {
    await this.equipamentoService.getAll().subscribe(response => {
      this.sourceEquipamentos = response;
      this.targetEquipamentos = [];
      this.primengConfig.ripple = true;
    });
  }

  protected executaPosSetItem(): void {

    if (this.resource.equipamentos.length > 0) {

        this.targetEquipamentos = [].concat(this.resource.equipamentos);

        this.sourceEquipamentos = this.sourceEquipamentos.filter(item => {

          const selecionados = this.resource.equipamentos;

          if (!selecionados.find(equipamento => equipamento.id === item.id)) {
              return true;
          }
        });
    }
  }
}


