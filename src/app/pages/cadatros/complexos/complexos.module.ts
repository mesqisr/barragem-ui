import { SharedModule } from './../../../shared/shared.module';
import { ComplexosRoutingModule } from './complexos-routing.module';
import { NgModule } from '@angular/core';

import {IMaskModule} from 'angular-imask';
import {ButtonModule} from 'primeng/button';
import {ComplexoFormComponent} from './complexo-form/complexo-form.component';
import {ComplexoListComponent} from './complexo-list/complexo-list.component';
import {EstruturasModule} from '../estruturas/estruturas.module';
import {StepsModule} from 'primeng/steps';
import {PickListModule} from 'primeng/picklist';

@NgModule({
  declarations: [ComplexoFormComponent, ComplexoListComponent],
  imports: [
    SharedModule,
    ComplexosRoutingModule,
    IMaskModule,
    ButtonModule,
    SharedModule,
    EstruturasModule,
    StepsModule,
    PickListModule
  ]
})
export class ComplexosModule { }
