import { Component } from '@angular/core';
import {BaseResourceListDirective} from '../../../../core/base/base-resource-list.directive';
import {ComplexoMinerario} from '../shared/complexo.model';
import {ComplexoService} from '../shared/complexo.service';
import {SituacaoEnum} from '../../../../shared/models/enums/situacao.enum';
import {ConfirmationService} from 'primeng/api';
import {FormularioService} from '../../../../shared/services/formulario.service';
import {RelatorioDto} from '../../../../shared/models/dto/relatorioDto';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-complexo-list',
  templateUrl: './complexo-list.component.html',
  styleUrls: ['./complexo-list.component.scss']
})
export class ComplexoListComponent extends BaseResourceListDirective<ComplexoMinerario> {

  public situacaoEnum = SituacaoEnum;

  constructor(protected formularioSerivce: FormularioService,
              protected complexoService: ComplexoService,
              protected confirmationService: ConfirmationService,
              private sanitizer: DomSanitizer) {
    super(formularioSerivce, confirmationService, complexoService);
  }

  public gerarRelatorio(idComplexo: number): void {

    this.complexoService.getRelatorio(idComplexo)
      .subscribe((response: RelatorioDto) => {
          const  blob = new Blob([response.relatorio], {type: 'application/pdf'});
          const fileURL: any = this.sanitizer.bypassSecurityTrustResourceUrl(URL.createObjectURL(blob));
          this.saveAs(fileURL.changingThisBreaksApplicationSecurity, `${response.nomeArquivo}.pdf`);
        },
        error => {});
  }

  private saveAs(file, name): void {
    const link = document.createElement('a');
    link.href = file;
    link.download = name;
    link.click();
  }
}
