import { Injectable, Injector } from '@angular/core';
import {Ativo} from './ativo.model';
import { BaseResourceService } from 'src/app/core/services/base-resource.service';

@Injectable({
  providedIn: 'root'
})
export class AtivoService extends BaseResourceService<Ativo> {

  constructor(protected injector: Injector) {
    super('/ativos', injector, Ativo.fromJson);
  }
}
