import {BaseResourceModelDirective} from '../../../../core/base/model/base-resource.model';

export class Ativo extends BaseResourceModelDirective {
  constructor(
    public id?: any,
    public titulo?: string
  ) {
    super();
  }

  static fromJson(jsonData: any): Ativo {
    return Object.assign(new Ativo() , jsonData);
  }

}
