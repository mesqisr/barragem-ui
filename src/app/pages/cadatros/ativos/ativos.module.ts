import { SharedModule } from './../../../shared/shared.module';
import { AtivosRoutingModule } from './ativos-routing.module';
import { NgModule } from '@angular/core';

import {IMaskModule} from 'angular-imask';
import {ButtonModule} from 'primeng/button';
import {AtivoListComponent} from './ativo-list/ativo-list.component';
import {AtivoFormComponent} from './ativo-form/ativo-form.component';

@NgModule({
    declarations: [AtivoListComponent, AtivoFormComponent],
    exports: [
        AtivoListComponent
    ],
    imports: [
        AtivosRoutingModule,
        IMaskModule,
        ButtonModule,
        SharedModule
    ]
})
export class AtivosModule { }
