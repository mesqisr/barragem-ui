import { Component, Injector, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import {Ativo} from '../shared/ativo.model';
import {AtivoService} from '../shared/ativo.service';
import {BaseResourceFormDirective} from '../../../../core/base/base-resource-form.directive';
import {BaseMiniCrudFormDirective} from '../../../../core/base/base-mini-crud-form.directive';

@Component({
  selector: 'app-ativo-form',
  templateUrl: './ativo-form.component.html',
  styleUrls: ['./ativo-form.component.scss']
})
export class AtivoFormComponent extends BaseMiniCrudFormDirective<Ativo> {

   constructor(protected ativoService: AtivoService,
               protected injector: Injector
           ) {
    super(injector, new Ativo(), ativoService, Ativo.fromJson);
  }

  protected buildResourceForm(): void {
     this.resourceForm = this.formBuilder.group({
       id : [null],
       titulo: [null, [Validators.required , Validators.maxLength(200)]]
    });
  }

  protected creationPageTitle(): string {
    return 'Cadastro de Novo Ativo';
  }

  protected editionPageTitle(): string {
    const ativoTitulo: string  = this.resource.titulo || '' ;
    return 'Editando Ativo : ' + ativoTitulo;
  }

  submitForm(validateForm = true, resetFormUponSuccess = true): void {
    this.setResource();
    this.submittingForm = true;
    this.convertToUpperCaseValues();

    if (validateForm) {
      this.markFormValidations(this.resourceForm);
      if (this.resourceForm.invalid) {
        return;
      }
    }
    if (!this.resource.id || this.resource.id === null && this.indexResource < 0) {
      this.createResource(resetFormUponSuccess);
      return;
    }

    this.updateResource(resetFormUponSuccess);
  }

}


