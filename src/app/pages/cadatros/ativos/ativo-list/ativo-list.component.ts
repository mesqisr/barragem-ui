import { Component } from '@angular/core';
import {BaseResourceListDirective} from '../../../../core/base/base-resource-list.directive';
import {AtivoService} from '../shared/ativo.service';
import {Ativo} from '../shared/ativo.model';
import {BaseMiniCrudListDirective} from '../../../../core/base/base-mini-crud-list.directive';
import {ConfirmationService} from 'primeng/api';
import {FormularioService} from '../../../../shared/services/formulario.service';

@Component({
  selector: 'app-estrutura-list',
  templateUrl: './ativo-list.component.html',
  styleUrls: ['./ativo-list.component.scss']
})
export class AtivoListComponent extends BaseMiniCrudListDirective<Ativo> {

  constructor(protected formularioSerivce: FormularioService , protected confirmationService: ConfirmationService, protected ativoService: AtivoService) {
    super(formularioSerivce, confirmationService, ativoService);
  }

}
