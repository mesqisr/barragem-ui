import { SharedModule } from './../../../shared/shared.module';
import { ClienteFormComponent } from './cliente-form/cliente-form.component';
import { ClienteListComponent } from './cliente-list/cliente-list.component';
import { ClientesRoutingModule } from './clientes-routing.module';
import { NgModule } from '@angular/core';

import {IMaskModule} from 'angular-imask';
import {ButtonModule} from 'primeng/button';

@NgModule({
  declarations: [ClienteFormComponent, ClienteListComponent],
  imports: [
    SharedModule,
    ClientesRoutingModule,
    IMaskModule,
    ButtonModule
  ]
})
export class ClientesModule { }
