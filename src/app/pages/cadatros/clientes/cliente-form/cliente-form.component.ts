import { ClienteService } from './../shared/cliente.service';
import { Cliente } from './../shared/cliente.model';
import { Component, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import {BaseResourceFormDirective} from '../../../../core/base/base-resource-form.directive';


@Component({
  selector: 'app-cliente-form',
  templateUrl: './cliente-form.component.html',
  styleUrls: ['./cliente-form.component.scss']
})
export class ClienteFormComponent extends BaseResourceFormDirective<Cliente> {

  constructor(protected clienteService: ClienteService, protected injector: Injector) {
    super(injector, new Cliente(), clienteService, Cliente.fromJson);
  }

  protected buildResourceForm(): void {
     this.resourceForm = this.formBuilder.group({
      id : [null],
      nome: [null, [Validators.required, Validators.minLength(5), Validators.maxLength(50)]],
      endereco : [null, [Validators.required, Validators.maxLength(50)]],
      telefone:  [null, [Validators.required, Validators.minLength(13), Validators.maxLength(13)]],
    });
  }

  protected creationPageTitle(): string {
    return 'Cadastro de Novo Cliente';
  }

  protected editionPageTitle(): string {
    const clienteDescricao: string  = this.resource.nome || '' ;
    return 'Editando Cliente : ' + clienteDescricao;
  }

}


