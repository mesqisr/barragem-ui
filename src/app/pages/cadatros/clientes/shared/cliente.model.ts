import {BaseResourceModelDirective} from '../../../../core/base/model/base-resource.model';

export class Cliente extends BaseResourceModelDirective {
  constructor(
    public id?: any,
    public nome?: string,
    public endereco?: string,
    public telefone?: string
  ) {
    super();
  }

  static fromJson(jsonData: any): Cliente {
    return Object.assign(new Cliente() , jsonData);
  }

}
