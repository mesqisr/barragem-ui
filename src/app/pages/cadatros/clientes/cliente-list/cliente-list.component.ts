import { ClienteService } from './../shared/cliente.service';
import { Cliente } from './../shared/cliente.model';
import { Component } from '@angular/core';
import {BaseResourceListDirective} from '../../../../core/base/base-resource-list.directive';
import {ComplexoService} from '../../complexos/shared/complexo.service';
import {ConfirmationService} from 'primeng/api';
import {FormularioService} from '../../../../shared/services/formulario.service';

interface Car {
  vin?;
  year?;
  brand?;
  color?;
  price?;
  saleDate?;
}

@Component({
  selector: 'app-cliente-list',
  templateUrl: './cliente-list.component.html',
  styleUrls: ['./cliente-list.component.scss']
})

export class ClienteListComponent extends BaseResourceListDirective<Cliente> {

  cars: Car[] = [];

  constructor(protected formularioService: FormularioService, protected clienteService: ComplexoService, protected confirmationService: ConfirmationService) {
    super(formularioService, confirmationService, clienteService);
    this.popularCar();
  }

popularCar() {

 const car = {
  vin : 'dsad231ff',
  year : '2012',
  brand : 'VW',
  color: 'Orange'
 }

 this.cars.push(car);

}


}
