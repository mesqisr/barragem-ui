import {ErrorHandler, LOCALE_ID, NgModule} from '@angular/core';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import {UsuarioService} from './shared/services/usuario.service';
import {StorageService} from './shared/services/storage.service';

import {AuthInterceptor} from './interceptors/auth.interceptor';
import {ApplicationErrorHandler} from './interceptors/error-handler';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BrowserModule} from '@angular/platform-browser';
import {AppLoginComponent} from './app.login.component';
import {ReactiveFormsModule} from '@angular/forms';
import {NgHttpLoaderModule} from 'ng-http-loader';

import { registerLocaleData } from '@angular/common';
import ptBr from '@angular/common/locales/pt';

registerLocaleData(ptBr);

@NgModule({
  declarations: [
    AppComponent,
    AppLoginComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    HttpClientModule,
    NgHttpLoaderModule.forRoot(),
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [
    {provide: LOCALE_ID, useValue: 'pt'},
    {provide: HTTP_INTERCEPTORS,  useClass: AuthInterceptor, multi: true},
    {provide: ErrorHandler, useClass: ApplicationErrorHandler},
    UsuarioService, StorageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
