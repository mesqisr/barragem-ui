import {HttpErrorResponse} from '@angular/common/http';
import {ErrorHandler, Injectable, Injector, NgZone} from '@angular/core';
import {MessageService} from '../shared/services/message.service';

@Injectable()
export class ApplicationErrorHandler extends ErrorHandler {
    constructor(private injector: Injector,
                private zone: NgZone,
                private messageService: MessageService) {
        super();
    }

  // tslint:disable-next-line:typedef
    handleError(errorResponse: HttpErrorResponse | any) {

        // debugger;

        if (errorResponse.promise && errorResponse.rejection) {

            if (errorResponse.rejection._body || errorResponse.rejection.error) {

                const error = errorResponse.rejection._body || JSON.stringify(errorResponse.rejection.error);

                errorResponse = JSON.parse(error);

                this.zone.run(() => {
                    switch (errorResponse.status) {
                        case 400:
                            // bad request
                            this.messageService.errorMessage(errorResponse.message, errorResponse.titulo);
                            break;
                        case 401:
                            // mandar pro login
                            break;
                        case 403:
                            // Não autorizado.
                            break;
                        case 404:
                            // recurso nao encontrado
                            break;
                    }
                });

            } else {
                // debugger;
                this.messageService.errorMessage(errorResponse.rejection);
            }
        }


        if (errorResponse instanceof HttpErrorResponse) {

        }

        super.handleError(errorResponse);
    }
}
