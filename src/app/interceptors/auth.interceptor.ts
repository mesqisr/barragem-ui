import {Injectable} from '@angular/core';
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {StorageService} from '../shared/services/storage.service';
import {STORAGE_KEYS} from '../shared/config/storage_keys.config';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    private CONTEXT_URL = '/barragem/v1';

    constructor(private storageService: StorageService) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        const N = `${this.CONTEXT_URL}`.length;

        const requestToAPI = req.url.substring(0, N) === `${this.CONTEXT_URL}`;

        if (requestToAPI) {
            const token = this.storageService.getStorage(STORAGE_KEYS.token);
            const authReq = req.clone({headers: req.headers.set('Authorization', 'Bearer ' + token)});
            return next.handle(authReq);
        } else {
            return next.handle(req);
        }
    }
}
