import {Directive, Input} from '@angular/core';
import {BaseMiniCrudFormDirective} from './base-mini-crud-form.directive';
import {BaseResourceListDirective} from './base-resource-list.directive';
import {BaseResourceModelDirective} from './model/base-resource.model';
import toastr from 'toastr';

@Directive()
export abstract class BaseMiniCrudListDirective<T extends BaseResourceModelDirective> extends BaseResourceListDirective<T> {

  @Input() miniCrudForm: BaseMiniCrudFormDirective<T>;

  get items(): Array<T> {
    return this.miniCrudForm.items;
  }

  protected updateResource(resource: T): void {
    this.miniCrudForm.handlerUpdateResorce(resource);
    this.formularioService.scrollToTop();
  }

  // tslint:disable-next-line:typedef
  protected deleteResource(resource: T) {

    this.miniCrudForm.loadingResources = true;
    this.baseResourceService.delete(resource.id).subscribe(response => {
      this.miniCrudForm.loadingResources = false;
      toastr.success('Solicitação processada com sucesso!');
      this.updateList(resource);
    }, error => {
      toastr.error('Ocorreu um erro ao processar a sua solicitação');
      this.miniCrudForm.loadingResources = false;
    });
  }

  protected disableActions(resource: T): boolean {
    if (this.miniCrudForm.currentResource) {
      if (this.miniCrudForm.currentResource.id === resource.id) {
        return true;
      }
    }
    return false;
  }

  private updateList(resource: T): void {
    this.miniCrudForm.handlerDeleteResorce(resource);
  }
}
