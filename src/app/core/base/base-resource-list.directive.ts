import { Directive, OnInit } from '@angular/core';
import { BaseResourceService } from '../services/base-resource.service';
import { BaseResourceModelDirective} from './model/base-resource.model';
import toastr from 'toastr';
import {ConfirmationService} from 'primeng/api';
import {FormularioService} from '../../shared/services/formulario.service';

@Directive()
export abstract class BaseResourceListDirective<T extends BaseResourceModelDirective> implements OnInit {

  public resources: T[];
  public loadingResources = false;
  constructor( protected formularioService: FormularioService,
               protected confirmationService: ConfirmationService,
               protected baseResourceService: BaseResourceService<T>) { }

  ngOnInit(): void {
    this.loadingResources = true;
    this.baseResourceService.getAll().subscribe(
      (resources => {
        this.loadingResources = false;
        debugger;
        this.resources = resources;
      }),
      (error: any) => {
        toastr.error('Ocorreu um erro ao carregar recurso.');
        this.loadingResources = false;
    });
    }


  protected confirmRemove(resource: T): void {

    this.confirmationService.confirm({
      message: 'Deseja realmente excluir esse registro ?',
      accept: () => {
        this.deleteResource(resource);
      }
    });

  }

  protected deleteResource(resource: T): void {
      this.loadingResources = true;
      this.baseResourceService.delete(resource.id).subscribe(
        () => {
          toastr.success('Solicitação processada com sucesso!');
          this.loadingResources = false;
          this.resources = this.resources.filter(element => element !== resource);
        },
        () => {
          toastr.error('Ocorreu um erro ao processar a sua solicitação');
          this.loadingResources = false;
        }
      );
  }
}
