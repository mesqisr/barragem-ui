import {OnInit, AfterContentChecked, Directive, Input} from '@angular/core';
import {BaseResourceFormDirective} from './base-resource-form.directive';
import {BaseResourceModelDirective} from './model/base-resource.model';
import toastr from 'toastr';

@Directive()
// tslint:disable-next-line:max-line-length
export abstract class BaseMiniCrudFormDirective<T extends BaseResourceModelDirective> extends BaseResourceFormDirective<T> implements OnInit, AfterContentChecked {

  public currentResource: T;
  public items: T[] = [];
  public loadingResources = false;
  public indexResource = -1;

  ngOnInit(): void {
    this.findAllItems();
    this.buildResourceForm();
  }

  ngAfterContentChecked(): void {
    this.setPageTitle();
  }

  protected findAllItems(): void {
    this.loadingResources = true;
    this.resourceService.getAll().subscribe(
      (resources => {
        this.loadingResources = false;
        this.items = resources;
      }),
      (error: any) => {
        toastr.error('Ocorreu um erro ao carregar recurso.');
        this.loadingResources = false;
      });
  }

  public handlerDeleteResorce(resource: T): void {
    this.indexResource = this.getIndexElementList(resource);
    this.items.splice(this.indexResource, 1);
  }

  public handlerUpdateResorce(resource: T): void {
    this.executaPreSetItem();
    this.indexResource = this.getIndexElementList(resource);
    this.resourceForm.patchValue(this.items[this.indexResource]);
    this.markFormValidations(this.resourceForm);
    this.setResource();
    this.executaPosSetItem();
  }

  submitForm(validateForm = true, resetFormUponSuccess = true): void {
    this.setResource();
    this.submittingForm = true;
    this.convertToUpperCaseValues();

    if (validateForm) {
      this.markFormValidations(this.resourceForm);
      if (this.resourceForm.invalid) {
        return;
      }
    }

    debugger;

    if (!this.resource.id && this.indexResource < 0) {
        this.createResource(resetFormUponSuccess);
        return;
    }

    this.updateResource(resetFormUponSuccess);
  }

  protected setPageTitle(): void  {
    if (this.resource && this.resource.id) {
      this.pageTitle = this.editionPageTitle();
      return;
    }
    this.pageTitle = this.creationPageTitle();
  }

  protected createResource(resetFormUponSuccess = true): void {
      const resource: T = this.jsonDataToResourceFn(this.resourceForm.value);
      this.loadingResources = true;
      this.resourceService.create(resource)
        .subscribe(
          (response) => {
            this.loadingResources = false;
            debugger;
            this.items = [].concat(this.items, response);
            this.actionsForSuccess(response, resetFormUponSuccess);
          },
          (error) => {
            this.loadingResources = false;
            this.actionsForError(error);
          }
        );
    }

  protected updateResource(resetFormUponSuccess = true): void {

      const resource: T = this.jsonDataToResourceFn(this.resourceForm.value);
      this.loadingResources = true;

      this.resourceService.update(resource)
        .subscribe(
          (response) => {
            this.loadingResources = false;
            this.indexResource = this.getIndexElementList(response);
            this.items.splice(this.indexResource, 1, response);
            this.items = [].concat(this.items);
            this.actionsForSuccess(resource, resetFormUponSuccess);
            this.loadingResources = false;
          },
          (error) => {
            this.actionsForError(error);
            this.loadingResources = false;
          }
        );
    }

  public cancelAction(): void {
    this.clearForm();
  }

  protected actionsForSuccess(resource: T | any, resetFormUponSuccess = true): void {
    toastr.success('Solicitação processada com sucesso!');
    if (resetFormUponSuccess) {
       this.clearForm();
    }
  }

  protected getIndexElementList(data: T): number {
    return this.items.indexOf(data);
  }

  protected setResource(): void {
    this.resource = this.resourceForm.value;
    this.currentResource = this.resourceForm.value;
  }

  protected clearForm(): void {
    this.resource = Object.create(null);
    this.currentResource = Object.create(null);
    this.resourceForm.reset();
    this.submittingForm = false;
    this.indexResource = -1;
  }

  @Input()
  public set item(value: T[]) {
    if (value) {
      this.items = value;
    }
  }
}
