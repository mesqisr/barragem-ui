import { HttpClient } from '@angular/common/http';
import { Injector } from '@angular/core';

import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { API } from 'src/app/config/variables';
import {BaseResourceModelDirective} from '../base/model/base-resource.model';

export abstract class BaseResourceService<T extends BaseResourceModelDirective> {

  protected http: HttpClient;
  private CONTEXT_URL = '/barragem';
  constructor(
    protected apiPath: string,
    protected injector: Injector,
    protected jsonDataToResourceFn: (jsonData: any) => T
  ) {
      this.http = injector.get(HttpClient);
}

  getAll(): Observable<T[]> {
    return this.http.get(`${this.CONTEXT_URL}/v1${this.apiPath}`).pipe(
      map(this.jsonDataToResources.bind(this)),
      catchError(this.handlerError)
    );
  }

  getById(id: number): Observable<T> {
    const url = `${this.CONTEXT_URL}/v1${this.apiPath}/${id}`;
    return this.http.get(url).pipe(
      map(this.jsonDataToResource.bind(this)),
      catchError(this.handlerError)
    );
  }

  create(resource: T): Observable<T> {
    return this.http.post(`${this.CONTEXT_URL}/v1${this.apiPath}`, resource).pipe(
     map(this.jsonDataToResource.bind(this)),
     catchError(this.handlerError)
    );
  }

  update(resource: T): Observable<T> {
    const url = `${this.CONTEXT_URL}/v1${this.apiPath}/${resource.id}`;
    return this.http.put(url, resource).pipe(
      map(() => resource),
      catchError(this.handlerError)
    );
  }

  delete(id: number): Observable<any> {
    const url = `${this.CONTEXT_URL}/v1${this.apiPath}/${id}`;
    return this.http.delete(url).pipe(
      map(() => null),
      catchError(this.handlerError)
    );
  }


  // PROTECTED METHODS
  protected jsonDataToResources(jsonData: any[]): T[] {
    const resources: T[] = [];
    jsonData.forEach(
        element => resources.push( this.jsonDataToResourceFn(element) )
      );
    return resources;
  }

  protected jsonDataToResource(jsonData: any): T {
    return this.jsonDataToResourceFn(jsonData);
  }

  protected handlerError(error: any): Observable<any> {
    console.log('ERROR NA REQUISIÇÃO => ', error);
    return throwError(error);
  }


}
