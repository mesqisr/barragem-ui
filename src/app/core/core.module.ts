import { SideBarComponent } from './layout/side-bar/side-bar.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { NavBarComponent } from './layout/nav-bar/nav-bar.component';
import { FooterComponent } from './layout/footer/footer.component';


@NgModule({
  declarations: [NavBarComponent, FooterComponent, SideBarComponent],
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule
  ],

  exports: [
    HttpClientModule,
    NavBarComponent,
    SideBarComponent,
    FooterComponent
  ]
})
export class CoreModule {}
