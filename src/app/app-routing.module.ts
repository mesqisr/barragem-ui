import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AppLoginComponent} from './app.login.component';

const routes: Routes = [

  {
    path: 'login',
    component: AppLoginComponent
  },

  {
    path: '',
    loadChildren: () => import('./app.main.module').then(m => m.AppMainModule),
  },

  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  }

  // {
  //  path: '**',
   // redirectTo: '/notfound'
  // }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
