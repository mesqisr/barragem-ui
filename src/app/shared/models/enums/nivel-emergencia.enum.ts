export enum NivelEmergenciaEnum {
  NIVEL_1 = 'Nível 1',
  NIVEL_2 = 'Nível 2',
  NIVEL_3 = 'Nível 3',
  SEM_EMERGENCIA = 'Sem Emergência'
}
