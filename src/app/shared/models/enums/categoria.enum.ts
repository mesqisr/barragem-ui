export enum CategoriaEnum {
  AUTOMACAO = 'Automação',
  BOMBAS_E_TUBULACOES = 'Bombas e Tubulações',
  ESCAVACAO_E_CARREGAMENTO = 'Escavação e Carregamento',
  EQUIPAMENTO_ELETRICO = 'Equipamento Elétrico',
  BRITAGEM_E_TRANSPORTADOR_POR_CORREIA = 'Britagem e Transportador por Correia'
}
