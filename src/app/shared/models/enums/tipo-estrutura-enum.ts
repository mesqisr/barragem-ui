export enum TipoEstruturaEnum {
  BARRAGEM = 'Barragem',
  MINA = 'Mina',
  ESTRTURA_ADMINISTRATIVA = 'Estrutura Administrativa',
  ESTRTURA_APOIO = 'Estrutura de Apoio'
}
