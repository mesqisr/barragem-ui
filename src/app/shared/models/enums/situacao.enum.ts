export enum SituacaoEnum {
  EM_OPERACAO = 'Em Operação',
  EM_CONSTRUCAO = 'Em Construção',
  DESATIVADA = 'Desativada'
}
