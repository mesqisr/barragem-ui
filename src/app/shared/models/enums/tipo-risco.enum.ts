export enum TipoRiscoEnum {
  ALTO = 'Alto',
  MEDIO = 'Médio',
  BAIXO = 'Baixo'
}
