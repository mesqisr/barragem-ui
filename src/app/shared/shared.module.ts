import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import { FormValidationContainerComponent } from './components/form-validation-container/form-validation-container.component';
import { PanelContainerComponent } from './components/panel-container/panel-container.component';

// PRIME NG
import {TableModule} from 'primeng/table';
import {ButtonModule} from 'primeng/button';
import {InputTextModule} from 'primeng/inputtext';
import {PanelModule} from 'primeng/panel';
import {ProgressBarModule} from 'primeng/progressbar';
import {DropdownModule} from 'primeng/dropdown';


import {ServerErrorMessagesComponent} from './components/server-error-messages/server-error-messages.component';
import {PageHeaderComponent} from './components/page-header/page-header.component';
import {BreadCrumbComponent} from './components/bread-crumb/bread-crumb.component';
import {CheckboxModule} from 'primeng/checkbox';
import { KeysPipe } from './pipes/keys.pipe';
import {FullCalendarModule} from 'primeng/fullcalendar';
import {TooltipModule} from 'primeng/tooltip';

@NgModule({
    declarations: [
        FormValidationContainerComponent,
        PanelContainerComponent,
        PageHeaderComponent,
        ServerErrorMessagesComponent,
        BreadCrumbComponent,
        KeysPipe
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        RouterModule,
        PanelModule,
        TableModule,
        TooltipModule,
        ButtonModule,
        InputTextModule,
        ProgressBarModule,
        DropdownModule,
        FullCalendarModule,
        CheckboxModule
    ],
  exports: [
    // shared Modules
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    TooltipModule,
    FormValidationContainerComponent,
    PanelContainerComponent,
    PageHeaderComponent,
    ServerErrorMessagesComponent,
    BreadCrumbComponent,

    // prime ng components
    PanelModule,
    TableModule,
    DropdownModule,
    ButtonModule,
    ProgressBarModule,
    InputTextModule,
    ServerErrorMessagesComponent,
    PageHeaderComponent,
    FullCalendarModule,
    CheckboxModule,

    // pipes
    KeysPipe

  ],
    providers : []
})

export class SharedModule {}
