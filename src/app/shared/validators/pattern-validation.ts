export class PatternValidation {

    static get numberPattern(): RegExp {
        return /^[0-9]*$/;
    }

    static get emailPattern(): RegExp {
        return /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    }
}
