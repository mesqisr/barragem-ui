import {STORAGE_KEYS} from '../config/storage_keys.config';
import {Injectable} from '@angular/core';

@Injectable({providedIn: 'root'})
export class StorageService {

    public setStorage(key: string, obj: any): void {
        sessionStorage.setItem(STORAGE_KEYS[key], JSON.stringify(obj));
    }

    public removeStorage(key: string): void {
        sessionStorage.removeItem(STORAGE_KEYS[key]);
    }

    public getStorage(key: string): any {
        const value = sessionStorage.getItem(STORAGE_KEYS[key]);

        if (value == null) {
            return null;
        }
        return JSON.parse(value);
    }
}
