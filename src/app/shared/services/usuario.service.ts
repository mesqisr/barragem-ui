import {Injectable, Injector} from '@angular/core';
import {STORAGE_KEYS} from '../config/storage_keys.config';
import {StorageService} from './storage.service';
import {Usuario} from '../models/usuario';
import {BaseResourceService} from '../../core/services/base-resource.service';
import {HttpClient} from '@angular/common/http';
import {API} from '../../config/variables';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  private usuarioLogado: Usuario;
  private URL = `${API}/auth`;
  constructor(private storageService: StorageService, private http: HttpClient) {

  }

  // tslint:disable-next-line:typedef
  public async login(login: any) {

    const result: any =  await this.http.post('/barragem/auth', login).toPromise()
      .catch(error => {
        return Promise.reject(error);
      });

    this.usuarioLogado = new Usuario();
    this.usuarioLogado.nome = result?.usuarioLogado.nome;
    this.usuarioLogado.id = result?.usuarioLogado.id;
    this.usuarioLogado.email = result?.usuarioLogado.username;

    return Promise.resolve({
      userData : this.usuarioLogado,
      token : result.token
    });
  }

  public getUsuarioLogado(): Usuario {
    if (!this.usuarioLogado) {
         this.usuarioLogado = this.storageService.getStorage(STORAGE_KEYS.userData);
    }
    return this.usuarioLogado;
  }

}
