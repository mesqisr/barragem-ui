import {Injectable} from '@angular/core';

import toastr from 'toastr';

@Injectable({
    providedIn: 'root'
})
export class MessageService {

    constructor() {

        toastr.options = {
            closeButton: true,
            debug: false,
            newestOnTop: false,
            progressBar: true,
            positionClass: 'toast-top-right',
            preventDuplicates: false,
            onclick: null,
            showDuration: '300',
            hideDuration: '10000',
            timeOut: '10000',
            extendedTimeOut: '10000',
            showEasing: 'swing',
            hideEasing: 'linear',
            showMethod: 'fadeIn',
            hideMethod: 'fadeOut'
        };

    }

    public infoMessage(message: string, titulo?: string): void {
        toastr.info(message, titulo);
    }

    public successMessage(message: string, titulo?: string): void {
        toastr.success(message, titulo);
    }

    public warningMessage(message: string, titulo?: string): void {
        toastr.warning(message, titulo);
    }

    public errorMessage(message: string, titulo?: string): void {
        toastr.error(message, titulo);
    }

}
