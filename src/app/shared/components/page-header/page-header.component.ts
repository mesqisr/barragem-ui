import { BreadCrumbItem } from './../../models/bread-crumb-Item';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-page-header',
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.scss']
})
export class PageHeaderComponent implements OnInit {


  @Input() breadCrumbItems: Array<BreadCrumbItem> = [];

  // tslint:disable-next-line: no-input-rename
  @Input('page-title') pageTitle: string;

  constructor() { }

  ngOnInit(): void {
  }

}
