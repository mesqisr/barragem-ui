import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import {PatternValidation} from '../../validators/pattern-validation';

@Component({
  selector: 'app-form-validation-container',
  templateUrl: './form-validation-container.component.html',
  styleUrls: ['./form-validation-container.component.scss']
})
export class FormValidationContainerComponent {

    // tslint:disable-next-line:no-input-rename
    @Input('form-control') formControl: FormControl;

    @Input() showTip = true;

    @Input() selectType = false;

    @Input() label: string;

    @Input() for: string;

    constructor() {}

    public get errorMessage(): string | null {
        if (this.mustShowErrorMessage()) {
            return this.getErrorMessage();
        } else {
            return null;
        }
    }

    private mustShowErrorMessage(): boolean {
        return this.formControl.invalid && (this.formControl.dirty || this.formControl.touched);
    }

    private getErrorMessage(): string | null {
        if (this.formControl.errors.required) {
            return 'Campo Obrigatório';

        } else if (this.formControl.errors.email) {
            return 'Formato de e-mail inválido';
        } else if (this.formControl.errors.dateInvalid) {
            return 'Formato de data inválido';

        } else if (this.formControl.errors.dateBefore) {
            const dataLimite = this.formControl.errors.dateBefore.dataLimite;
            return `A data deve ser posterior à ${dataLimite} `;
        } else if (this.formControl.errors.minlength) {
            const requiredLength = this.formControl.errors.minlength.requiredLength;
            return `O campo deve ter no mínimo ${requiredLength} caracteres`;

        } else if (this.formControl.errors.maxlength) {
            const requiredLength = this.formControl.errors.maxlength.requiredLength;
            return `O campo deve ter no máximo ${requiredLength} caracteres`;

        } else if (this.formControl.errors.pattern) {

            const numberFormat = PatternValidation.numberPattern.toString();

            if (this.formControl.errors.pattern.requiredPattern === numberFormat) {
                return `Valor inválido`;
            }
        }
    }

    hasSuccess(): boolean {
        return this.formControl.valid && (this.formControl.dirty || this.formControl.touched);
    }

    hasError(): boolean {
        return this.formControl.invalid && (this.formControl.dirty || this.formControl.touched);
    }

}
