import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormValidationContainerComponent } from './form-validation-container.component';

describe('FormValidationContainerComponent', () => {
  let component: FormValidationContainerComponent;
  let fixture: ComponentFixture<FormValidationContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormValidationContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormValidationContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
