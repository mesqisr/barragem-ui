import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import { UsuarioService } from '../shared/services/usuario.service';
import {STORAGE_KEYS} from '../shared/config/storage_keys.config';
import {StorageService} from '../shared/services/storage.service';

@Injectable({
    providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

    constructor(private storageService: StorageService, private router: Router) {
    }

    async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {

        if (this.usuarioLogado()) {
            return true;
        }
    }

    private usuarioLogado(): boolean {
        return this.storageService.getStorage(STORAGE_KEYS.userData) != null;
   }

}
