import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {UsuarioService} from './shared/services/usuario.service';
import {StorageService} from './shared/services/storage.service';
import {STORAGE_KEYS} from './shared/config/storage_keys.config';
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './app.login.component.html',
  styleUrls: ['./app.login.component.scss']
})
export class AppLoginComponent implements OnInit {

  public loginForm: FormGroup;

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private usuarioService: UsuarioService,
              private storageService: StorageService) {

  }

  ngOnInit(): void {

    this.loginForm = this.formBuilder.group({
      email: [null, [Validators.required, Validators.email]],
      senha: [null, [Validators.required]]
    });
  }


  async acaoLogin(): Promise<void> {

    try {

      const usuario = this.loginForm.value;

      const logado = await this.usuarioService.login(usuario);

      if (logado !== null && logado !== undefined) {
        this.loginSuccess(logado);
      }

    } catch (e) {
      Promise.reject(e);
    }

  }

  private loginSuccess(logado): void {
    this.storageService.setStorage(STORAGE_KEYS.userData, logado.userData);
    this.storageService.setStorage(STORAGE_KEYS.token, logado.token);
    this.router.navigate(['/dashboard']);
  }

  public errorMessage(formControl: AbstractControl): string | null {

    if (this.hasError(formControl)) {
      if (formControl.errors.required) {
        return 'Campo Obrigatório';

      } else if (formControl.errors.email) {
        return 'Formato de e-mail inválido';
      }
    }
    return null;
  }

  hasError(formControl: AbstractControl): boolean {
    return formControl.invalid && (formControl.dirty || formControl.touched);
  }

  hasSuccess(formControl: AbstractControl): boolean {
    return formControl.valid && (formControl.dirty || formControl.touched);
  }
}
