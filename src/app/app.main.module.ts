import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {AppMainComponent} from './app.main.component';
import {SharedModule} from './shared/shared.module';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ConfirmationService} from 'primeng/api';
import {MessageService} from './shared/services/message.service';
import {CoreModule} from './core/core.module';
import {AuthGuardService} from './guard/auth-guard.service';

const routes: Routes = [

  {
    path: '',
    component: AppMainComponent,
    canActivate: [AuthGuardService],
      children: [
         {
              path: 'dashboard',
              canActivate: [AuthGuardService],
              loadChildren: () => import('./pages/dashboard/dashboard.module').then(d => d.DashboardModule)
          },

        {
          path: 'complexos',
          loadChildren:  () => import('./pages/cadatros/complexos/complexos.module').then(c => c.ComplexosModule)
        },

        {
          path: 'equipamentos',
          loadChildren:  () => import('./pages/cadatros/equipamentos/equipamentos.module').then(e => e.EquipamentosModule)
        },

        {
          path: 'ativos',
          loadChildren:  () => import('./pages/cadatros/ativos/ativos.module').then(a => a.AtivosModule)
        },

        {
          path: 'agendas',
          loadChildren:  () => import('./pages/cadatros/agendas/agendas.module').then(c => c.AgendasModule)
        },

        {
          path: 'clientes',
          loadChildren:  () => import('./pages/cadatros/clientes/clientes.module').then(c => c.ClientesModule)
        },

          /*{
              path: '',
              redirectTo: '/login',
              pathMatch: 'full'
          }*/
      ],
  },
 {
      path: '',
      redirectTo: '/dashboard',
      pathMatch: 'full'
  }
];

@NgModule({
    declarations: [
        AppMainComponent,
    ],
    imports: [
       CoreModule,
       ConfirmDialogModule,
       RouterModule.forChild(routes),
       SharedModule
    ], exports : [
        AppMainComponent,
    ], providers: [
        ConfirmationService,
        MessageService
    ]
})
export class AppMainModule {
}
