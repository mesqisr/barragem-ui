# Pré-requisitos

Para começar a utilizar o projeto Barragem UI é pré-requisito ter o Node.js instalado (versão 10.x ou acima) e o seu gerenciador de pacote favorito na versão mais atual. Caso você ainda não tenha instalado o pacote @angular/cli, instale-o via npm ou yarn.

Instalando com npm:

`npm i -g @angular/cli@^10`

Caso prefira instalar com o yarn:

`yarn global add @angular/cli@^10`

## Instalando Depedencias
Depois de instalado o angular CLI basta entrar na pasta do projeto clonado e executar um `npm i` para o node instalar as dependecias do projeto. 

## Executando o servidor

Execute o comando `npm start` para iniciar o servidor de desenvolvimento. Navegue até `http://localhost:4200/#/login`. O aplicativo será recarregado automaticamente.

## Build

Execute `ng build` para construir o projeto. Os artefatos de construção serão armazenados no diretório `dist /`. Use o sinalizador `--prod` para uma construção de produção.

